package cfg;

import java.util.HashSet;
import java.util.Set;

import cfg.sideeffects.SideEffect;

public class ReturnTrans extends Transition {

	protected ReturnTrans(FunctionImplementation funcImpl) {
		super(funcImpl.getReturnNode(), funcImpl);
	}

	@Override
	public void printYourself(TRFileManager fm, int pc, int tPC) {
		assert (getCondition() == null);
		addSideEffect(new SideEffect(funcImpl.getPC(),
				funcImpl.getReturnAddress()));
		Set<Variable> usedVars = new HashSet<Variable>();
		for (SideEffect se : getSideEffects()) {
			fm.getTrWriter().format(
					"%d;;%s;%s;%s%n",
					pc,
					se.getVar().toSMTLIB2(),
					se.getRepr().toSMTLIB2(),
					se.getFunction() != null ? se.getFunction().toSMTLIB2()
							: "");
			usedVars.add(se.getVar());
		}
		printRemainingVars(fm, pc, "", usedVars);
	}
}
