package cfg.sideeffects;

import cfg.Function;
import cfg.Variable;
import cfg.arrays.ArrayFunction;
import cfg.arrays.ArrayVariable;
import cfg.cond.BoolEquals;
import cfg.cond.BoolFunction;
import cfg.cond.BoolVariable;
import cfg.function.IntFunction;
import cfg.function.IntVariable;

public class SideEffect {

	protected Variable var = null;
	protected BoolFunction repr = null;
	protected Function f = null;

	public Variable getVar() {
		return var;
	}

	public BoolFunction getRepr() {
		return repr;
	}

	public Function getFunction() {
		return f;
	}

	protected SideEffect() {
	}

	public SideEffect(IntVariable var, IntFunction f) {
		this.var = var;
		this.f = f;
		repr = new BoolEquals(var.getPrime(), f);
	}

	public SideEffect(BoolVariable var, BoolFunction f) {
		this.var = var;
		this.f = f;
		repr = new BoolEquals(var.getPrime(), f);
	}

	public SideEffect(ArrayVariable var, ArrayFunction f) {
		this.var = var;
		this.f = f;
		repr = new BoolEquals(var.getPrime(), f);
	}

	public SideEffect(BoolFunction repr, Variable var) {
		this.repr = repr;
		this.var = var;
	}

}
