package cfg.sideeffects;

import cfg.Variable;
import cfg.cond.BoolTrue;

public class NondetSideEffect extends SideEffect {

	public NondetSideEffect(Variable v) {
		super.var = v;
		super.repr = new BoolTrue();
		super.f = null;
	}
	
}
