package cfg;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class Main {

	public static void main(String[] args) throws IOException {
		final File f = new File(args[0]);
		if (!f.exists()) {
			System.err.println("Can't find file " + f.getAbsolutePath());
			return;
		}

		CharStream stream = new ANTLRFileStream(args[0]);
		TRGen2Lexer lex = new TRGen2Lexer(stream);

		AtomicBoolean cont = new AtomicBoolean(true);

		lex.addErrorListener(new DescriptiveErrorListener(cont));
		CommonTokenStream tokens = new CommonTokenStream(lex);
		for (int i = 0; i < tokens.size(); i++) {
			System.out.println(tokens.get(i).getText());
		}

		TRGen2Parser parser = new TRGen2Parser(tokens);
		parser.addErrorListener(new DescriptiveErrorListener(cont));
		// BailErrorStrategy errorHandler = new BailErrorStrategy();
		// parser.setErrorHandler(errorHandler);
		ParserRuleContext tree = null;
		try {
			tree = parser.prog();
			if (cont.get()) {
				ParseTreeWalker walker = new ParseTreeWalker();
				MyTRGen2Listener extractor = new MyTRGen2Listener(f);
				System.out.println("digraph CFG {");
				walker.walk(extractor, tree);
				System.out.println("}");
			} else {
				System.err.println("Aborting translation because"
						+ " of lexer/parser errors");
			}
		} catch (ParseCancellationException e) {
			System.err.println("Aborting translation because"
					+ " of lexer/parser errors");
		}
	}
}
