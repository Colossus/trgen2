package cfg;

import java.util.concurrent.atomic.AtomicBoolean;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class DescriptiveErrorListener extends BaseErrorListener {

	private AtomicBoolean cont;

	public DescriptiveErrorListener(AtomicBoolean cont) {
		this.cont = cont;
	}

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		String sourceName = recognizer.getInputStream().getSourceName();
		sourceName = String.format("%s:%d:%d: ", sourceName, line,
				charPositionInLine);
		System.err.println(sourceName + " " + msg);
		cont.set(false);
	}
}