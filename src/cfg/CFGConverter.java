package cfg;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cfg.cond.BoolEquals;
import cfg.cond.BoolFalse;
import cfg.fcall.FunctionCall;
import cfg.function.SMTInteger;
import cfg.sideeffects.SideEffect;

public class CFGConverter {

	private FunctionImplementation impl;

	public CFGConverter(FunctionImplementation impl) {
		this.impl = impl;
	}

	private int pcCounter;

	public void print(TRFileManager fm) {
		HashMap<Node, Integer> nodeIndices = new HashMap<Node, Integer>();
		nodeIndices.put(impl.getReturnNode(), 0);
		fm.getArgsWriter().println(0);
		Set<Node> visited = new HashSet<Node>();
		System.out.println("subgraph " + impl.getName() + "{ ");
		System.out.println("label=\"" + impl.getName() + "\";");
		pcCounter = 1;
		impl.addInitial(new BoolEquals(impl.getPC(), new SMTInteger(pcCounter)));
		printNode(fm, impl.getEntryNode(), pcCounter, visited, nodeIndices);
		System.out.println("}");
	}

	private void printNode(TRFileManager fm, Node n, int pc, Set<Node> visited,
			Map<Node, Integer> nodeIndices) {
		if (visited.contains(n)) {
			return;
		}
		assert (pc != -1);
		visited.add(n);
		if (n.getForbiddenCondition() != null) {
			if (n.getForbiddenCondition() instanceof BoolFalse) {
				fm.getAsrtsWriter().println("(not (= .s " + pc + "))");
			} else {
				fm.getAsrtsWriter().println(
						"(=> (= .s " + pc + ")"
								+ n.getForbiddenCondition().toSMTLIB2() + ")");
			}
		}
		assert (n.getTransitions().size() > 0) : n.getComment();
		for (Transition t : n.getTransitions()) {
			int tPC;
			if (nodeIndices.containsKey(t.getNext())) {
				tPC = nodeIndices.get(t.getNext());
			} else {
				pcCounter++;
				tPC = pcCounter;
				nodeIndices.put(t.getNext(), tPC);
			}
			if (impl.getCall(n) == null) {
				t.printYourself(fm, pc, tPC);
				printDot(impl.getName(), t, pc, tPC);
			} else {
				printFunctionCall(fm, n, pc, tPC);
				t.printProgress(fm, pc, tPC);
				printDotProgress(impl.getName(), t, pc, tPC);
			}
			printNode(fm, t.getNext(), tPC, visited, nodeIndices);
		}
	}

	private boolean printFunctionCall(TRFileManager fm, Node n, int pc, int tPC) {
		FunctionCall call = impl.getCall(n);
		assert (call != null);
		PrintWriter out = fm.getFCallWriter();
		out.print(pc + " " + tPC + " " + call.getCalledName() + " ");
		if (call.getAssignedVariable() != null) {
			out.println(call.getAssignedVariable().toSMTLIB2());
		} else {
			out.println();
		}
		for (StateVariable arg : call.getArgs()) {
			out.println(arg.toSMTLIB2());
		}
		out.println();
		return true;
	}

	private void printDot(String fName, Transition t, int pc, int tPC) {
		System.out.print(fName + pc + " -> " + fName + tPC + " [label=\"");
		if (t instanceof ReturnTrans) {
			System.out.print("(return) ");
		}
		if (t.getCondition() != null) {
			System.out.print("|" + t.getCondition().toSMTLIB2() + "| ");
		}
		for (SideEffect s : t.getSideEffects()) {
			if (s.getFunction() != null) {
				System.out.print(s.getVar().toSMTLIB2() + "'="
						+ s.getFunction().toSMTLIB2() + "; ");
			} else {
				assert (s.getRepr() != null);
				System.out.print(s.getRepr().toSMTLIB2().replace("_p", "'")
						+ "; ");
			}
		}
		System.out.println("\"];");
	}

	private void printDotProgress(String fName, Transition t, int pc, int tPC) {
		System.out.print(fName + pc + " -> " + fName + tPC + " [label=\"");
		assert (!(t instanceof ReturnTrans));
		assert (t.getCondition() == null);
		System.out.print("s'=" + tPC);
		System.out.println("\"];");
	}
}
