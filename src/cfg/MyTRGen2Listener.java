package cfg;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;

import cfg.TRGen2Parser.ArrayResizeContext;
import cfg.TRGen2Parser.ArrayStoreContext;
import cfg.TRGen2Parser.AssertStatementContext;
import cfg.TRGen2Parser.AssignmentContext;
import cfg.TRGen2Parser.AssumeStatementContext;
import cfg.TRGen2Parser.BoolDeclarationContext;
import cfg.TRGen2Parser.BreakStatementContext;
import cfg.TRGen2Parser.ContinueStatementContext;
import cfg.TRGen2Parser.DeclarationContext;
import cfg.TRGen2Parser.DeclarationIdContext;
import cfg.TRGen2Parser.DynArrayDeclarationContext;
import cfg.TRGen2Parser.ElseBlockContext;
import cfg.TRGen2Parser.FixedArrayDeclarationContext;
import cfg.TRGen2Parser.FunctionCallContext;
import cfg.TRGen2Parser.FunctionDefArgsContext;
import cfg.TRGen2Parser.FunctionDefinitionContext;
import cfg.TRGen2Parser.IfBlockContext;
import cfg.TRGen2Parser.IntDeclarationContext;
import cfg.TRGen2Parser.LvalContext;
import cfg.TRGen2Parser.MainContext;
import cfg.TRGen2Parser.NondetAssignmentContext;
import cfg.TRGen2Parser.ProgAssignmentContext;
import cfg.TRGen2Parser.ProgContext;
import cfg.TRGen2Parser.ReturnStatementContext;
import cfg.TRGen2Parser.TermContext;
import cfg.TRGen2Parser.TestContext;
import cfg.TRGen2Parser.WhileStatementContext;
import cfg.arrays.ArrayInputVar;
import cfg.arrays.ArrayStore;
import cfg.arrays.ArrayVariable;
import cfg.arrays.DynArrayVariable;
import cfg.arrays.FixedArrayVariable;
import cfg.cond.BoolAnd;
import cfg.cond.BoolComp;
import cfg.cond.BoolEquals;
import cfg.cond.BoolFunction;
import cfg.cond.BoolVariable;
import cfg.cond.ForAll;
import cfg.function.ArrayAccess;
import cfg.function.IntFunction;
import cfg.function.IntVariable;
import cfg.function.SMTInteger;
import cfg.sideeffects.SideEffect;

public class MyTRGen2Listener extends TRGen2BaseListener {

	private FunctionImplementation curImpl = null;
	private HashMap<String, FunctionImplementation> impls = new HashMap<String, FunctionImplementation>();
	private File f;

	public MyTRGen2Listener(File f) {
		this.f = f;
	}

	@Override
	public void exitProg(@NotNull ProgContext ctx) {
	}

	@Override
	public void enterMain(@NotNull MainContext ctx) {
		curImpl = new FunctionImplementation("main");
		impls.put("main", curImpl);
	}

	@Override
	public void enterFunctionDefinition(@NotNull FunctionDefinitionContext ctx) {
		String fName = ctx.getChild(1).getText();
		curImpl = new FunctionImplementation(fName);
		impls.put(fName, curImpl);
		ParseTree possibleArgs = ctx.getChild(3);
		if (possibleArgs instanceof FunctionDefArgsContext) {
			curImpl.parseArgs((FunctionDefArgsContext) possibleArgs);
		}
	}

	@Override
	public void exitMain(@NotNull MainContext ctx) {
		curImpl.proceed();
		curImpl.returnWith(curImpl.getCurState().createIntInput());
		try {
			curImpl.printYourself(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		curImpl = null;
	}

	@Override
	public void exitFunctionDefinition(@NotNull FunctionDefinitionContext ctx) {
		curImpl.proceed();
		curImpl.returnWith(curImpl.getCurState().createIntInput());
		try {
			curImpl.printYourself(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		curImpl = null;
	}

	@Override
	public void enterReturnStatement(@NotNull ReturnStatementContext ctx) {
		curImpl.proceed();
		IntFunction retVal = curImpl
				.evaluateTerm((TermContext) ctx.getChild(1));
		curImpl.returnWith(retVal);
	}

	@Override
	public void enterNondetAssignment(@NotNull NondetAssignmentContext ctx) {
		curImpl.proceed();
		Set<SideEffect> se = new HashSet<SideEffect>();
		if (ctx.getChild(0) instanceof LvalContext) {
			String var = ctx.getChild(0).getText();
			Variable declVar = curImpl.lookupDeclVar(var);
			if (declVar instanceof IntVariable) {
				se.add(new SideEffect((IntVariable) declVar, curImpl
						.getCurState().createIntInput()));
			} else if (declVar instanceof BoolVariable) {
				se.add(new SideEffect((BoolVariable) declVar,
						(BoolFunction) curImpl.getCurState().createBoolInput()));
			}
		} else {
			String var = ctx.getChild(0).getChild(0).getText();
			Variable declVar = curImpl.lookupDeclVar(var);
			se.add(new SideEffect((ArrayVariable) declVar, new ArrayStore(
					(ArrayVariable) declVar, new SMTInteger(ctx.getChild(0)
							.getChild(2).getText()), curImpl.getCurState()
							.createIntInput())));
		}
		curImpl.amend("nondet assignment", se);
	}

	@Override
	public void enterFunctionCall(@NotNull FunctionCallContext ctx) {
		curImpl.proceed();
		curImpl.amendFunctionCall(ctx);
	}

	@Override
	public void enterProgAssignment(@NotNull ProgAssignmentContext ctx) {
		curImpl.proceed();
		Set<SideEffect> sideEffects = new HashSet<SideEffect>();
		curImpl.evaluateAssignment((AssignmentContext) ctx.getChild(0),
				sideEffects);
		curImpl.amend("assignment of " + ctx.getChild(0).getText(), sideEffects);
	}

	@Override
	public void enterArrayStore(@NotNull ArrayStoreContext ctx) {
		curImpl.proceed();
		Set<SideEffect> sideEffects = new HashSet<SideEffect>();
		IntFunction idx = curImpl.evaluateTerm((TermContext) ctx.getChild(0)
				.getChild(2));
		IntFunction val = curImpl.evaluateTerm((TermContext) ctx.getChild(2));
		Variable var = curImpl.lookupDeclVar(ctx.getChild(0).getChild(0)
				.getText());
		sideEffects.add(new SideEffect((ArrayVariable) var, new ArrayStore(
				(ArrayVariable) var, idx, val)));
		curImpl.amend("array store", sideEffects);
	}

	@Override
	public void enterWhileStatement(@NotNull WhileStatementContext ctx) {
		curImpl.proceed();
		BoolFunction ret;
		if (ctx.getChild(2).getText().equals("*")) {
			ret = curImpl.getCurState().createBoolInput();
		} else {
			ret = curImpl.evaluateTest((TestContext) ctx.getChild(2));
		}
		curImpl.amendWhile(ret);
	}

	@Override
	public void exitWhileStatement(@NotNull WhileStatementContext ctx) {
		curImpl.proceed();
		curImpl.exitWhile();
	}

	@Override
	public void enterBreakStatement(@NotNull BreakStatementContext ctx) {
		curImpl.proceed();
		curImpl.breakWhile();
	}

	@Override
	public void enterContinueStatement(@NotNull ContinueStatementContext ctx) {
		curImpl.proceed();
		curImpl.continueWhile();
	}

	@Override
	public void enterAssumeStatement(@NotNull AssumeStatementContext ctx) {
		curImpl.proceed();
		BoolFunction ret = curImpl.evaluateTest((TestContext) ctx.getChild(2));
		curImpl.amendAssume(ret);
	}

	@Override
	public void enterAssertStatement(@NotNull AssertStatementContext ctx) {
		curImpl.proceed();
		BoolFunction ret = curImpl.evaluateTest((TestContext) ctx.getChild(2));
		curImpl.amendAssert(ret);
	}

	@Override
	public void enterIfBlock(@NotNull IfBlockContext ctx) {
		curImpl.proceed();
		BoolFunction ret;
		if (ctx.getChild(2).getText().equals("*")) {
			ret = curImpl.getCurState().createBoolInput();
		} else {
			ret = curImpl.evaluateTest((TestContext) ctx.getChild(2));
		}
		curImpl.amendIf(ret);
	}

	@Override
	public void exitIfBlock(@NotNull IfBlockContext ctx) {
		if (ctx.getParent().getChildCount() == 1) {
			curImpl.proceed();
			curImpl.exitIf();
		} else {
			curImpl.exitIfElse();
		}
	}

	@Override
	public void enterElseBlock(@NotNull ElseBlockContext ctx) {
		curImpl.enterElse();
	}

	@Override
	public void exitElseBlock(@NotNull ElseBlockContext ctx) {
		curImpl.proceed();
		curImpl.exitElse();
	}

	@Override
	public void enterArrayResize(@NotNull ArrayResizeContext ctx) {
		curImpl.proceed();
		String lVar = ctx.getChild(0).getText();
		Variable v = curImpl.lookupDeclVar(lVar);
		assert (v instanceof ArrayVariable);
		ArrayVariable av = (ArrayVariable) v;
		Set<SideEffect> sideEffects = new HashSet<SideEffect>();
		IntFunction term = curImpl.evaluateTerm((TermContext) ctx.getChild(4));
		if (ctx.getChild(2).getText().equals("malloc")) {
			BoolFunction cond1 = new BoolEquals(av.getBound(),
					new SMTInteger(0));
			BoolFunction cond2 = new BoolComp(">", term, new SMTInteger(0));
			curImpl.getCurState().setSafetyCondition(new BoolAnd(cond1, cond2));
			sideEffects.add(new SideEffect(av, curImpl.getCurState()
					.createArrayInput(term)));
		} else {
			BoolFunction cond1 = new BoolComp(">", av.getBound(),
					new SMTInteger(0));
			BoolFunction cond2 = new BoolComp(">", term, av.getBound());
			curImpl.getCurState().setSafetyCondition(new BoolAnd(cond1, cond2));
			IntVariable fa1 = new IntVariable(".forall.1");
			BoolFunction bounds = new BoolAnd(new BoolComp("<=",
					new SMTInteger(0), fa1), new BoolComp("<", fa1,
					av.getBound()));
			BoolFunction cons = new BoolEquals(new ArrayAccess(av.getPrime(),
					fa1), new ArrayAccess(av, fa1));
			ArrayInputVar arrayInputVar = curImpl.getCurState()
					.createArrayInput(term);
			sideEffects.add(new SideEffect(new ForAll(fa1, bounds, cons), av));
			BoolFunction bounds2 = new BoolAnd(new BoolComp("<=",
					av.getBound(), fa1), new BoolComp("<", fa1, term));
			BoolFunction cons2 = new BoolEquals(new ArrayAccess(av.getPrime(),
					fa1), new ArrayAccess(arrayInputVar, fa1));
			sideEffects
					.add(new SideEffect(new ForAll(fa1, bounds2, cons2), av));
		}
		sideEffects.add(new SideEffect((IntVariable) av.getBound(), term));
		curImpl.amend("array resize", sideEffects);
	};

	VarType type;
	SMTInteger fixedArraySize = null;

	@Override
	public void enterDeclarationId(@NotNull DeclarationIdContext ctx) {
		String varName;
		if (curImpl == null) {
			varName = ctx.getText();
		} else {
			varName = curImpl.getName() + "." + ctx.getText();
		}
		StateVariable var;
		if (type.equals(VarType.INT)) {
			var = new IntVariable(varName);
		} else if (type.equals(VarType.BOOL)) {
			var = new BoolVariable(varName);
		} else if (type.equals(VarType.FIXED_ARRAY)) {
			var = new FixedArrayVariable(varName, fixedArraySize);
		} else if (type.equals(VarType.DYN_ARRAY)) {
			var = new DynArrayVariable(varName);
			assert (curImpl != null);
			curImpl.addInitial(new BoolEquals(((DynArrayVariable) var)
					.getBound(), new SMTInteger(0)));
			curImpl.addStateVariable((IntVariable) ((DynArrayVariable) var)
					.getBound());
		} else {
			assert (false) : "declaration id can only be int or bool";
			var = null;
		}
		curImpl.addDeclaredVariable(ctx.getText(), var);
	}

	@Override
	public void enterDeclaration(@NotNull DeclarationContext ctx) {
		if (ctx.getChild(0) instanceof IntDeclarationContext) {
			type = VarType.INT;
		} else if (ctx.getChild(0) instanceof BoolDeclarationContext) {
			type = VarType.BOOL;
		} else if (ctx.getChild(0) instanceof FixedArrayDeclarationContext) {
			type = VarType.FIXED_ARRAY;
			fixedArraySize = new SMTInteger(ctx.getChild(0).getChild(1)
					.getText());
		} else if (ctx.getChild(0) instanceof DynArrayDeclarationContext) {
			type = VarType.DYN_ARRAY;
		} else {
			assert (false);
		}
	}

}
