grammar TRGen2;

@header {
    package cfg;
}

INT          		: [0-9]+ ;
WHITESPACE   		: ('\t'|' '|'\r'|'\n'|'\u000C')+    -> channel(HIDDEN) ;
ID		      		: [a-z_][a-z0-9_]* ;
COMMENT      		: '/*' .*? '*/' -> skip ;
LINE_COMMENT 		: '//' ~('\n'|'\r')* '\r'? '\n' -> skip ;
NONDET				: '*' ;
INCR				: '++' ;
DECR				: '--' ;

varNames			: declarationId
					| declarationId ',' varNames
					;
declarationId		: id ;

declaration			: intDeclaration
					| boolDeclaration
					| fixedArrayDeclaration
					| dynArrayDeclaration
					;
intDeclaration			: 'int' varNames ';' ;
boolDeclaration			: 'bool' varNames ';' ;
fixedArrayDeclaration	: 'int[' INT ']' varNames ';' ;
dynArrayDeclaration		: 'int[' ']' varNames ';' ;

functionName        : id ;
argName             : id ;
functionDefArgs     : 'int' argName ',' functionDefArgs
                    | 'int' argName
                    ;
functionDefinition  : 'int' functionName '(' functionDefArgs ')' '{' declaration* statement+ '}'
                    | 'int' functionName '(' ')' '{' declaration* statement+ '}'
                    ;
main				: 'int' 'main' '(' ')' '{' declaration* statement+ '}' ;
id					: ID ;
prog        		: (functionDefinition)* main ;
returnStatement		: 'return' term ';' ;
statement   		: ifStatement
            		| whileStatement
					| progAssignment
					| nondetAssignment
					| functionCall
					| ';'
					| '{' statement+ '}'
					| returnStatement
					| breakStatement
					| continueStatement
					| assertStatement
					| assumeStatement
					| arrayStore
					| arrayResize
					;
					
functionCall        : lval '=' functionName '(' callArgs ')' ';'
                    | lval '=' functionName '(' ')' ';'
                    ;
callArgs            : id ',' callArgs
                    | id
                    ;
breakStatement		: 'break' ';' ;
continueStatement	: 'continue' ';' ;
progAssignment		: assignment ';' ;
ifStatement 		: ifBlock elseBlock? ;
ifBlock				: 'if' '(' test ')' statement
					| 'if' '(' '*' ')' statement
					;
elseBlock			: 'else' statement ;
whileStatement 		: 'while' '(' test ')' statement
					| 'while' '(' '*' ')' statement
					;
assertStatement 	: 'assert' '(' test ')' ';' ;
assumeStatement		: 'assume' '(' test ')' ';' ;
lval				: id ;
arrayAccess			: id '[' term ']' ;
nondetAssignment    : lval '=' NONDET 
					| arrayAccess '=' NONDET
					;
assignment			: lval '=' term
					| lval '=' test
					| lval '=' assignment
			    	| incr lval
			    	| lval incr
			    	| decr lval
			    	| lval decr
					;
arrayStore			: arrayAccess '=' term ';' ;
arrayResize			: id '=' 'malloc' '(' term ')' ';'
					| id '=' 'realloc' '(' term ')' ';'
					;
test				: id
					| term
               		| term ('<'|'>'|'=='|'!='|'<='|'>=') term
            		| term '==' term '%' integer
                	| '!' test 
                	| test ('&&'|'||') test
                	| '(' test ')'
                	| 'true'
                	| 'false'
                	;
incr				: INCR ;
decr				: DECR ;
integer				: INT ;
term				: id
			    	| integer
					| arrayAccess
			    	| '-' term
			    	| term ('*') term
			    	| term ('+'|'-') term
			    	| '(' term ')'
			   		;