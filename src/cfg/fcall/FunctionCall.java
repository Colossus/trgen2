package cfg.fcall;

import java.util.List;

import cfg.StateVariable;
import cfg.function.IntVariable;

public class FunctionCall {

	private String calledName = null;
	private List<StateVariable> args = null;
	private IntVariable assignedVariable = null;

	public FunctionCall(String calledName, IntVariable assignedVariable,
			List<StateVariable> callArgs) {
		this.calledName = calledName;
		this.assignedVariable = assignedVariable;
		this.args = callArgs;
	}

	public String getCalledName() {
		return calledName;
	}

	public List<StateVariable> getArgs() {
		return args;
	}

	public IntVariable getAssignedVariable() {
		return assignedVariable;
	}

}
