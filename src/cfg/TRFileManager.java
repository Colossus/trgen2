package cfg;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TRFileManager {

	private final File varsFile;
	private final File trFile;
	private final File asrtsFile;
	private final File nondetFile;
	private final File initFile;
	private final File trxFile;
	private final File fCallFile;
	private final File argsFile;

	private final PrintWriter varsFileWriter;
	private final PrintWriter trFileWriter;
	private final PrintWriter asrtsFileWriter;
	private final PrintWriter nondetFileWriter;
	private final PrintWriter initFileWriter;
	private final PrintWriter trxFileWriter;
	private final PrintWriter fCallFileWriter;
	private final PrintWriter argsFileWriter;

	public TRFileManager(File f, String functionName) throws IOException {
		varsFile = new File(f.getAbsolutePath() + "-" + functionName + ".vars");
		trFile = new File(f.getAbsolutePath() + "-" + functionName + ".tr");
		asrtsFile = new File(f.getAbsolutePath() + "-" + functionName
				+ ".asrts");
		nondetFile = new File(f.getAbsolutePath() + "-" + functionName
				+ ".nondet");
		initFile = new File(f.getAbsolutePath() + "-" + functionName + ".init");
		trxFile = new File(f.getAbsoluteFile() + "-" + functionName + ".trx");
		fCallFile = new File(f.getAbsoluteFile() + "-" + functionName
				+ ".fcalls");
		argsFile = new File(f.getAbsoluteFile() + "-" + functionName + ".args");

		if (varsFile.exists()) {
			varsFile.delete();
			varsFile.createNewFile();
		}
		if (trFile.exists()) {
			trFile.delete();
			trFile.createNewFile();
		}
		if (asrtsFile.exists()) {
			asrtsFile.delete();
			asrtsFile.createNewFile();
		}
		if (nondetFile.exists()) {
			nondetFile.delete();
			nondetFile.createNewFile();
		}
		if (initFile.exists()) {
			initFile.delete();
			initFile.createNewFile();
		}
		if (trxFile.exists()) {
			trxFile.delete();
			trxFile.createNewFile();
		}
		if (fCallFile.exists()) {
			fCallFile.delete();
			fCallFile.createNewFile();
		}
		if (argsFile.exists()) {
			argsFile.delete();
			argsFile.createNewFile();
		}

		varsFileWriter = new PrintWriter(varsFile);
		trFileWriter = new PrintWriter(trFile);
		asrtsFileWriter = new PrintWriter(asrtsFile);
		nondetFileWriter = new PrintWriter(nondetFile);
		initFileWriter = new PrintWriter(initFile);
		trxFileWriter = new PrintWriter(trxFile);
		fCallFileWriter = new PrintWriter(fCallFile);
		argsFileWriter = new PrintWriter(argsFile);
	}

	public PrintWriter getInitWriter() {
		return initFileWriter;
	}

	public PrintWriter getAsrtsWriter() {
		return asrtsFileWriter;
	}

	public PrintWriter getTrWriter() {
		return trFileWriter;
	}

	public PrintWriter getNondetWriter() {
		return nondetFileWriter;
	}

	public PrintWriter getVarWriter() {
		return varsFileWriter;
	}

	public PrintWriter getTrxWriter() {
		return trxFileWriter;
	}

	public PrintWriter getFCallWriter() {
		return fCallFileWriter;
	}

	public PrintWriter getArgsWriter() {
		return argsFileWriter;
	}

	public void close() {
		varsFileWriter.close();
		trFileWriter.close();
		asrtsFileWriter.close();
		nondetFileWriter.close();
		initFileWriter.close();
		trxFileWriter.close();
		fCallFileWriter.close();
		argsFileWriter.close();
	}

}
