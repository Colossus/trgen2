// Generated from TRGen2.g4 by ANTLR 4.1

    package cfg;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TRGen2Parser}.
 */
public interface TRGen2Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#decr}.
	 * @param ctx the parse tree
	 */
	void enterDecr(@NotNull TRGen2Parser.DecrContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#decr}.
	 * @param ctx the parse tree
	 */
	void exitDecr(@NotNull TRGen2Parser.DecrContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#varNames}.
	 * @param ctx the parse tree
	 */
	void enterVarNames(@NotNull TRGen2Parser.VarNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#varNames}.
	 * @param ctx the parse tree
	 */
	void exitVarNames(@NotNull TRGen2Parser.VarNamesContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#test}.
	 * @param ctx the parse tree
	 */
	void enterTest(@NotNull TRGen2Parser.TestContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#test}.
	 * @param ctx the parse tree
	 */
	void exitTest(@NotNull TRGen2Parser.TestContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#progAssignment}.
	 * @param ctx the parse tree
	 */
	void enterProgAssignment(@NotNull TRGen2Parser.ProgAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#progAssignment}.
	 * @param ctx the parse tree
	 */
	void exitProgAssignment(@NotNull TRGen2Parser.ProgAssignmentContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#assumeStatement}.
	 * @param ctx the parse tree
	 */
	void enterAssumeStatement(@NotNull TRGen2Parser.AssumeStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#assumeStatement}.
	 * @param ctx the parse tree
	 */
	void exitAssumeStatement(@NotNull TRGen2Parser.AssumeStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(@NotNull TRGen2Parser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(@NotNull TRGen2Parser.ReturnStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#elseBlock}.
	 * @param ctx the parse tree
	 */
	void enterElseBlock(@NotNull TRGen2Parser.ElseBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#elseBlock}.
	 * @param ctx the parse tree
	 */
	void exitElseBlock(@NotNull TRGen2Parser.ElseBlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#assertStatement}.
	 * @param ctx the parse tree
	 */
	void enterAssertStatement(@NotNull TRGen2Parser.AssertStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#assertStatement}.
	 * @param ctx the parse tree
	 */
	void exitAssertStatement(@NotNull TRGen2Parser.AssertStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#id}.
	 * @param ctx the parse tree
	 */
	void enterId(@NotNull TRGen2Parser.IdContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#id}.
	 * @param ctx the parse tree
	 */
	void exitId(@NotNull TRGen2Parser.IdContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(@NotNull TRGen2Parser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(@NotNull TRGen2Parser.ProgContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(@NotNull TRGen2Parser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(@NotNull TRGen2Parser.FunctionCallContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#arrayResize}.
	 * @param ctx the parse tree
	 */
	void enterArrayResize(@NotNull TRGen2Parser.ArrayResizeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#arrayResize}.
	 * @param ctx the parse tree
	 */
	void exitArrayResize(@NotNull TRGen2Parser.ArrayResizeContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#callArgs}.
	 * @param ctx the parse tree
	 */
	void enterCallArgs(@NotNull TRGen2Parser.CallArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#callArgs}.
	 * @param ctx the parse tree
	 */
	void exitCallArgs(@NotNull TRGen2Parser.CallArgsContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#lval}.
	 * @param ctx the parse tree
	 */
	void enterLval(@NotNull TRGen2Parser.LvalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#lval}.
	 * @param ctx the parse tree
	 */
	void exitLval(@NotNull TRGen2Parser.LvalContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#dynArrayDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterDynArrayDeclaration(@NotNull TRGen2Parser.DynArrayDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#dynArrayDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitDynArrayDeclaration(@NotNull TRGen2Parser.DynArrayDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(@NotNull TRGen2Parser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(@NotNull TRGen2Parser.DeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void enterIfBlock(@NotNull TRGen2Parser.IfBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void exitIfBlock(@NotNull TRGen2Parser.IfBlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(@NotNull TRGen2Parser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(@NotNull TRGen2Parser.IntegerContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#arrayStore}.
	 * @param ctx the parse tree
	 */
	void enterArrayStore(@NotNull TRGen2Parser.ArrayStoreContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#arrayStore}.
	 * @param ctx the parse tree
	 */
	void exitArrayStore(@NotNull TRGen2Parser.ArrayStoreContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#functionDefArgs}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefArgs(@NotNull TRGen2Parser.FunctionDefArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#functionDefArgs}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefArgs(@NotNull TRGen2Parser.FunctionDefArgsContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#fixedArrayDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFixedArrayDeclaration(@NotNull TRGen2Parser.FixedArrayDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#fixedArrayDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFixedArrayDeclaration(@NotNull TRGen2Parser.FixedArrayDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(@NotNull TRGen2Parser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(@NotNull TRGen2Parser.ContinueStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(@NotNull TRGen2Parser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(@NotNull TRGen2Parser.MainContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(@NotNull TRGen2Parser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(@NotNull TRGen2Parser.IfStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#arrayAccess}.
	 * @param ctx the parse tree
	 */
	void enterArrayAccess(@NotNull TRGen2Parser.ArrayAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#arrayAccess}.
	 * @param ctx the parse tree
	 */
	void exitArrayAccess(@NotNull TRGen2Parser.ArrayAccessContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull TRGen2Parser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull TRGen2Parser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(@NotNull TRGen2Parser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(@NotNull TRGen2Parser.AssignmentContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(@NotNull TRGen2Parser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(@NotNull TRGen2Parser.FunctionDefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#boolDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterBoolDeclaration(@NotNull TRGen2Parser.BoolDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#boolDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitBoolDeclaration(@NotNull TRGen2Parser.BoolDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(@NotNull TRGen2Parser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(@NotNull TRGen2Parser.TermContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(@NotNull TRGen2Parser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(@NotNull TRGen2Parser.WhileStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#nondetAssignment}.
	 * @param ctx the parse tree
	 */
	void enterNondetAssignment(@NotNull TRGen2Parser.NondetAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#nondetAssignment}.
	 * @param ctx the parse tree
	 */
	void exitNondetAssignment(@NotNull TRGen2Parser.NondetAssignmentContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#intDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterIntDeclaration(@NotNull TRGen2Parser.IntDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#intDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitIntDeclaration(@NotNull TRGen2Parser.IntDeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#declarationId}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationId(@NotNull TRGen2Parser.DeclarationIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#declarationId}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationId(@NotNull TRGen2Parser.DeclarationIdContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#incr}.
	 * @param ctx the parse tree
	 */
	void enterIncr(@NotNull TRGen2Parser.IncrContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#incr}.
	 * @param ctx the parse tree
	 */
	void exitIncr(@NotNull TRGen2Parser.IncrContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(@NotNull TRGen2Parser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(@NotNull TRGen2Parser.BreakStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(@NotNull TRGen2Parser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(@NotNull TRGen2Parser.FunctionNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link TRGen2Parser#argName}.
	 * @param ctx the parse tree
	 */
	void enterArgName(@NotNull TRGen2Parser.ArgNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TRGen2Parser#argName}.
	 * @param ctx the parse tree
	 */
	void exitArgName(@NotNull TRGen2Parser.ArgNameContext ctx);
}