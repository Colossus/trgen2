package cfg;

import java.util.HashSet;
import java.util.Set;

import cfg.cond.BoolEquals;
import cfg.cond.BoolFunction;
import cfg.cond.BoolInputVar;
import cfg.function.SMTInteger;
import cfg.sideeffects.SideEffect;

public class Transition {

	private BoolFunction condition;
	private Set<SideEffect> sideEffects;
	protected Node next;
	protected FunctionImplementation funcImpl;

	protected Transition(BoolFunction condition, Node next,
			FunctionImplementation funcImpl) {
		this(next, funcImpl);
		this.condition = condition;
	}

	protected Transition(Node next, FunctionImplementation funcImpl) {
		assert (next != null);
		this.next = next;
		this.funcImpl = funcImpl;
		sideEffects = new HashSet<SideEffect>();
	}

	public Node getNext() {
		assert (next != null);
		return next;
	}

	public void addSideEffects(Set<SideEffect> se) {
		sideEffects.addAll(se);
	}

	public void addSideEffect(SideEffect se) {
		sideEffects.add(se);
	}

	public BoolFunction getCondition() {
		return condition;
	}

	public Set<SideEffect> getSideEffects() {
		return sideEffects;
	}

	public void printYourself(TRFileManager fm, int pc, int tPC) {
		String cond = "";
		if (getCondition() != null) {
			cond = getCondition().toSMTLIB2();
			if (!(getCondition() instanceof BoolInputVar)) {
				fm.getTrxWriter().println(cond);
			}
		}
		Set<Variable> usedVars = new HashSet<Variable>();
		addSideEffect(new SideEffect(funcImpl.getPC(), new SMTInteger(tPC)));
		for (SideEffect se : getSideEffects()) {
			fm.getTrWriter().format(
					"%d;%s;%s;%s;%s%n",
					pc,
					cond,
					se.getVar().toSMTLIB2(),
					se.getRepr().toSMTLIB2(),
					se.getFunction() != null ? se.getFunction().toSMTLIB2()
							: "");
			usedVars.add(se.getVar());
		}
		printRemainingVars(fm, pc, cond, usedVars);
	}

	public void printProgress(TRFileManager fm, int pc, int tPC) {
		SideEffect se = new SideEffect(funcImpl.getPC(), new SMTInteger(tPC));
		assert (getCondition() == null);
		fm.getTrWriter().format("%d;%s;%s;%s;%s%n", pc, "",
				funcImpl.getPC().toSMTLIB2(), se.getRepr().toSMTLIB2(),
				se.getFunction().toSMTLIB2());
	}

	protected void printRemainingVars(TRFileManager fm, int pc, String cond,
			Set<Variable> usedVars) {
		assert (pc != -1);
		for (StateVariable v : funcImpl.getStateVariables()) {
			if (usedVars.contains(v)) {
				continue;
			}
			fm.getTrWriter().format("%d;%s;%s;%s;%s%n", pc, cond,
					v.toSMTLIB2(), new BoolEquals(v.getPrime(), v).toSMTLIB2(),
					v.toSMTLIB2());
		}
	}
}
