package cfg;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cfg.arrays.ArrayInputVar;
import cfg.cond.BoolFunction;
import cfg.cond.BoolInputVar;
import cfg.cond.BoolNot;
import cfg.function.IntFunction;
import cfg.function.IntInputVar;
import cfg.sideeffects.SideEffect;

public class Node {

	private BoolFunction forbiddenCondition;
	private List<Transition> transitions = new ArrayList<Transition>();
	private String comment;
	protected FunctionImplementation impl;

	int intInputVarIdx = 0;
	int boolInputVarIdx = 0;
	int arrayInputVarIdx = 0;

	public Node(String comment, FunctionImplementation impl) {
		this.comment = comment;
		this.impl = impl;
	}

	public IntInputVar createIntInput() {
		IntInputVar ret = new IntInputVar(intInputVarIdx);
		impl.addInputVar(ret);
		intInputVarIdx++;
		return ret;
	}

	public BoolInputVar createBoolInput() {
		BoolInputVar ret = new BoolInputVar(boolInputVarIdx);
		impl.addInputVar(ret);
		boolInputVarIdx++;
		return ret;
	}

	public ArrayInputVar createArrayInput(IntFunction size) {
		ArrayInputVar ret = new ArrayInputVar(arrayInputVarIdx, size);
		impl.addInputVar(ret);
		arrayInputVarIdx++;
		return ret;
	}

	private void addTransition(Transition trans) {
		assert (trans != null);
		assert (trans.getNext() != null);
		transitions.add(trans);
	}

	public BoolFunction getForbiddenCondition() {
		return forbiddenCondition;
	}

	public void setSafetyCondition(BoolFunction condition) {
		this.forbiddenCondition = condition;
	}

	public List<Transition> getTransitions() {
		return transitions;
	}

	public String getComment() {
		return comment;
	}

	public Transition createTrans(String nextNodeName, Set<SideEffect> se) {
		Node next = new Node(nextNodeName, impl);
		Transition trans = new Transition(next, impl);
		addTransition(trans);
		trans.addSideEffects(se);
		return trans;
	}

	public Transition createTrans(Node next) {
		Transition trans = new Transition(next, impl);
		addTransition(trans);
		return trans;
	}

	public ReturnTrans createReturnTrans(IntFunction ret,
			FunctionImplementation impl) {
		ReturnTrans trans = new ReturnTrans(impl);
		addTransition(trans);
		trans.addSideEffect(new SideEffect(impl.getReturnVariable(), ret));
		return trans;
	}

	public TransitionPair createTrans(BoolFunction pos, String node1Name,
			String node2Name) {
		Node next1 = new Node(node1Name, impl);
		Node next2 = new Node(node2Name, impl);
		Transition trans1 = new Transition(pos, next1, impl);
		BoolFunction neg = new BoolNot(pos);
		Transition trans2 = new Transition(neg, next2, impl);
		addTransition(trans1);
		addTransition(trans2);
		TransitionPair ret = new TransitionPair();
		ret.trueTrans = trans1;
		ret.falseTrans = trans2;
		return ret;
	}
}
