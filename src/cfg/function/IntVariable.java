package cfg.function;

import cfg.StateVariable;

public class IntVariable implements IntFunction, StateVariable {

	private String plain;

	public IntVariable(String plain) {
		this.plain = plain;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() != IntVariable.class)
			return false;
		IntVariable other = (IntVariable) obj;
		return other.plain.equals(plain);
	}

	@Override
	public int hashCode() {
		return plain.hashCode();
	}

	@Override
	public String toSMTLIB2() {
		return plain;
	}

	@Override
	public String getPrintDeclaration() {
		return "int " + this.toSMTLIB2();
	}

	@Override
	public IntVariable getPrime() {
		return new IntVariable(plain + "_p");
	}

	@Override
	public StateVariable copy(String newName) {
		return new IntVariable(newName);
	}

}
