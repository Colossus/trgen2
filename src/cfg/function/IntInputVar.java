package cfg.function;

import cfg.InputVar;

public class IntInputVar implements IntFunction, InputVar {

	Integer num = null;

	public IntInputVar(int inputVarIdx) {
		this.num = inputVarIdx;
	}

	@Override
	public String toSMTLIB2() {
		if (num == null) {
			assert (false);
			System.err
					.println("Assertion failure, must set input var num before printing");
			System.exit(1);
		}
		return ".inputVar.int." + num;
	}

	@Override
	public String getPrintDeclaration() {
		return "int " + toSMTLIB2();
	}

}
