package cfg.function;

import cfg.Function;

public class UnaryMinus implements IntFunction {

	private Function fun1;

	public UnaryMinus(Function fun1) {
		this.fun1 = fun1;
	}

	@Override
	public String toSMTLIB2() {
		return "(- " + fun1.toSMTLIB2() + ")";
	}

}
