package cfg.function;

import cfg.arrays.ArrayFunction;

public class ArrayAccess implements IntFunction {

	private ArrayFunction arr;
	private IntFunction fun;

	public ArrayAccess(ArrayFunction arr, IntFunction fun2) {
		this.arr = arr;
		this.fun = fun2;
	}

	@Override
	public String toSMTLIB2() {
		return "(select " + arr.toSMTLIB2() + " " + fun.toSMTLIB2() + ")";
	}

}
