package cfg.function;

public class SMTInteger implements IntFunction {

	private int i;

	public SMTInteger(String integer) {
		i = Integer.parseInt(integer);
	}

	public SMTInteger(int i) {
		this.i = i;
	}

	@Override
	public String toSMTLIB2() {
		return new Integer(i).toString();
	}

}
