package cfg.arrays;

import cfg.StateVariable;
import cfg.function.IntFunction;

public abstract class ArrayVariable extends ArrayFunction implements
		StateVariable {

	protected String plain;
	protected IntFunction size;

	public ArrayVariable(String plain) {
		this.plain = plain;
	}

	@Override
	public String toString() {
		return plain;
	}

	public IntFunction getBound() {
		return size;
	}

	// necessary b/c of stronger return type
	@Override
	public abstract ArrayVariable getPrime();

}
