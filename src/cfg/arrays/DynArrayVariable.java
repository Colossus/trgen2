package cfg.arrays;

import cfg.StateVariable;
import cfg.function.IntVariable;

public class DynArrayVariable extends ArrayVariable {

	public DynArrayVariable(String plain) {
		super(plain);
		this.size = new IntVariable(plain + ".size");
	}

	@Override
	public String getPrintDeclaration() {
		return "dyn_array " + this.toSMTLIB2() + " " + size.toSMTLIB2();
	}

	@Override
	public DynArrayVariable getPrime() {
		DynArrayVariable ret = new DynArrayVariable(plain + "_p");
		ret.size = new IntVariable(plain + ".size_p");
		return ret;
	}

	@Override
	public String toSMTLIB2() {
		return plain;
	}

	@Override
	public StateVariable copy(String newName) {
		return new DynArrayVariable(newName);
	}

}
