package cfg.arrays;

import cfg.InputVar;
import cfg.function.IntFunction;

public class ArrayInputVar extends ArrayFunction implements InputVar {

	Integer num = null;
	IntFunction size;

	public ArrayInputVar(int inputVarIdx, IntFunction size) {
		this.num = inputVarIdx;
		this.size = size;
	}

	@Override
	public String toSMTLIB2() {
		if (num == null) {
			assert (false);
			System.err
					.println("Assertion failure, must set input var num before printing");
			System.exit(1);
		}
		return ".inputVar.array." + num;
	}

	@Override
	public String getPrintDeclaration() {
		return "input_array " + toSMTLIB2() + " " + size.toSMTLIB2();
	}

}
