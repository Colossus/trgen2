package cfg.arrays;

import cfg.StateVariable;
import cfg.function.SMTInteger;

public class FixedArrayVariable extends ArrayVariable {

	public FixedArrayVariable(String plain, SMTInteger size) {
		super(plain);
		this.size = size;
	}

	@Override
	public String getPrintDeclaration() {
		return "fixed_array " + this.toSMTLIB2() + " " + size.toSMTLIB2();
	}

	@Override
	public FixedArrayVariable getPrime() {
		return new FixedArrayVariable(plain + "_p", (SMTInteger) size);
	}

	@Override
	public String toSMTLIB2() {
		return plain;
	}

	@Override
	public StateVariable copy(String newName) {
		return new FixedArrayVariable(newName, (SMTInteger) size);
	}

}
