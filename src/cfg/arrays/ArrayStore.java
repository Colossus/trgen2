package cfg.arrays;

import cfg.function.IntFunction;

public class ArrayStore extends ArrayFunction {

	private ArrayVariable arr;
	private IntFunction idx;
	private IntFunction val;

	public ArrayStore(ArrayVariable arr, IntFunction idx, IntFunction val) {
		this.arr = arr;
		this.idx = idx;
		this.val = val;
	}

	@Override
	public String toSMTLIB2() {
		return "(store " + arr.toSMTLIB2() + " " + idx.toSMTLIB2() + " " + val.toSMTLIB2() + ")";
	}

}
