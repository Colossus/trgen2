package cfg.cond;


public class BoolFalse implements BoolFunction {

	public BoolFalse() {
	}

	@Override
	public String toSMTLIB2() {
		return "false";
	}

}
