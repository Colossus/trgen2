package cfg.cond;

import cfg.InputVar;

public class BoolInputVar implements BoolFunction, InputVar {

	Integer num = null;

	public BoolInputVar(int inputVarIdx) {
		this.num = inputVarIdx;
	}

	@Override
	public String toSMTLIB2() {
		if (num == null) {
			assert (false);
			System.err
					.println("Assertion failure, must set input var num before printing");
			System.exit(1);
		}
		return ".inputVar.bool." + num;
	}

	@Override
	public String getPrintDeclaration() {
		return "bool " + toSMTLIB2();
	}

}
