package cfg.cond;


public class BoolOr implements BoolFunction {

	private BoolFunction fun1;
	private BoolFunction fun2;

	public BoolOr(BoolFunction fun1, BoolFunction fun2) {
		this.fun1 = fun1;
		this.fun2 = fun2;
	}

	@Override
	public String toSMTLIB2() {
		return "(or " + fun1.toSMTLIB2() + " " + fun2.toSMTLIB2() + ")";
	}

}
