package cfg.cond;

import cfg.StateVariable;

public class BoolVariable implements BoolFunction, StateVariable {

	private String plain;

	public BoolVariable(String plain) {
		this.plain = plain;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() != BoolVariable.class) {
			return false;
		}
		BoolVariable other = (BoolVariable) obj;
		return other.plain.equals(plain);
	}

	@Override
	public int hashCode() {
		return plain.hashCode();
	}

	@Override
	public String toSMTLIB2() {
		return plain;
	}

	@Override
	public String getPrintDeclaration() {
		return "bool " + this.toSMTLIB2();
	}

	@Override
	public BoolVariable getPrime() {
		return new BoolVariable(plain + "_p");
	}

	@Override
	public StateVariable copy(String newName) {
		return new BoolVariable(newName);
	}

}
