package cfg.cond;


public class BoolTrue implements BoolFunction {

	public BoolTrue() {
	}

	@Override
	public String toSMTLIB2() {
		return "true";
	}

}
