package cfg.cond;


public class BoolNot implements BoolFunction {

	private BoolFunction fun;

	public BoolNot(BoolFunction fun) {
		this.fun = fun;
	}

	@Override
	public String toSMTLIB2() {
		return "(not " + fun.toSMTLIB2() + ")";
	}

}
