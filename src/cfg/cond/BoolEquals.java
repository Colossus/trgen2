package cfg.cond;

import cfg.Function;

public class BoolEquals implements BoolFunction {

	private Function fun1;
	private Function fun2;

	public BoolEquals(Function fun1, Function fun2) {
		this.fun1 = fun1;
		this.fun2 = fun2;
	}

	@Override
	public String toSMTLIB2() {
		return "(= " + fun1.toSMTLIB2() + " " + fun2.toSMTLIB2() + ")";
	}

}
