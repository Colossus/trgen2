package cfg.cond;

import cfg.function.IntFunction;

public class BoolComp implements BoolFunction {

	private String op;
	private IntFunction fun1;
	private IntFunction fun2;

	public BoolComp(String op, IntFunction fun1, IntFunction fun2) {
		this.op = op;
		this.fun1 = fun1;
		this.fun2 = fun2;
	}

	@Override
	public String toSMTLIB2() {
		return "(" + op + " " + fun1.toSMTLIB2() + " " + fun2.toSMTLIB2() + ")";
	}

}
