package cfg.cond;

import cfg.function.IntFunction;
import cfg.function.SMTInteger;

public class BoolDivisible implements BoolFunction {

	private SMTInteger modulo;
	private IntFunction lrv;
	private IntFunction rrv;

	public BoolDivisible(SMTInteger modulo, IntFunction lrv, IntFunction rrv) {
		this.modulo = modulo;
		this.lrv = lrv;
		this.rrv = rrv;
	}

	@Override
	public String toSMTLIB2() {
		return "((_ divisible " + modulo.toSMTLIB2() + ") (- " + lrv.toSMTLIB2()
				+ " " + rrv.toSMTLIB2() + "))";
	}

}
