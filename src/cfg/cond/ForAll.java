package cfg.cond;

import cfg.function.IntVariable;

public class ForAll implements BoolFunction {

	IntVariable iterator;
	BoolFunction range;
	BoolFunction cons;

	public ForAll(IntVariable it, BoolFunction range, BoolFunction cons) {
		this.iterator = it;
		this.range = range;
		this.cons = cons;
	}

	@Override
	public String toSMTLIB2() {
		return "(forall ((" + iterator.toSMTLIB2() + " Int)) (=> "
				+ range.toSMTLIB2() + " " + cons.toSMTLIB2() + "))";
	}

}
