package cfg;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import cfg.TRGen2Parser.ArgNameContext;
import cfg.TRGen2Parser.ArrayAccessContext;
import cfg.TRGen2Parser.AssignmentContext;
import cfg.TRGen2Parser.CallArgsContext;
import cfg.TRGen2Parser.DecrContext;
import cfg.TRGen2Parser.FunctionCallContext;
import cfg.TRGen2Parser.FunctionDefArgsContext;
import cfg.TRGen2Parser.IdContext;
import cfg.TRGen2Parser.IncrContext;
import cfg.TRGen2Parser.IntegerContext;
import cfg.TRGen2Parser.TermContext;
import cfg.TRGen2Parser.TestContext;
import cfg.arrays.ArrayVariable;
import cfg.arrays.DynArrayVariable;
import cfg.cond.BoolAnd;
import cfg.cond.BoolComp;
import cfg.cond.BoolDivisible;
import cfg.cond.BoolEquals;
import cfg.cond.BoolFalse;
import cfg.cond.BoolFunction;
import cfg.cond.BoolNot;
import cfg.cond.BoolOr;
import cfg.cond.BoolTrue;
import cfg.cond.BoolVariable;
import cfg.fcall.FunctionCall;
import cfg.function.ArrayAccess;
import cfg.function.IntFunction;
import cfg.function.IntVariable;
import cfg.function.Plus;
import cfg.function.SMTInteger;
import cfg.function.Times;
import cfg.function.UnaryMinus;
import cfg.sideeffects.NondetSideEffect;
import cfg.sideeffects.SideEffect;

public class FunctionImplementation {

	private String name;

	private LinkedList<Node> ifStack = new LinkedList<Node>();
	private LinkedList<StatePair> whileStack = new LinkedList<StatePair>();
	private Map<String, StateVariable> varsToDecl = new HashMap<String, StateVariable>();
	private Set<InputVar> inputVars = new HashSet<InputVar>();
	private Set<StateVariable> stateVariables = new HashSet<StateVariable>();
	private LinkedList<BoolFunction> initial = new LinkedList<BoolFunction>();
	private Map<Node, FunctionCall> calls = new HashMap<Node, FunctionCall>();

	private Node curState = null;
	private Node nextState = new Node("entry node", this);
	private Node returnNode = new Node("return node", this);
	private IntVariable pc = new IntVariable(".s");
	private static final SMTInteger returnAddress = new SMTInteger(0);
	private Node entryNode;
	private IntVariable returnVariable;
	private List<Variable> args = new ArrayList<Variable>();

	public FunctionImplementation(String name) {
		this.name = name;
		this.entryNode = nextState;
		returnVariable = new IntVariable(name + ".return.var");
		addStateVariable(returnVariable);
		addStateVariable(pc);
		returnNode.createTrans(returnNode);
	}

	Variable lookupDeclVar(String var) {
		Variable declVar = varsToDecl.get(var);
		assert (declVar != null) : "Variable " + var + " unknown";
		return declVar;
	}

	IntFunction evaluateTerm(TermContext term) {
		if (term.getChildCount() == 1) {
			if (term.getChild(0) instanceof ArrayAccessContext) {
				IdContext id = (IdContext) term.getChild(0).getChild(0);
				Variable var = lookupDeclVar(id.getText());
				assert (var instanceof ArrayVariable);
				IntFunction idx = evaluateTerm((TermContext) term.getChild(0)
						.getChild(2));
				curState.setSafetyCondition(new BoolAnd(new BoolComp(">=", idx,
						new SMTInteger(0)), new BoolComp("<", idx,
						((ArrayVariable) var).getBound())));
				return new ArrayAccess((ArrayVariable) var, idx);
			} else if (term.getChild(0) instanceof IdContext) {
				IdContext id = (IdContext) term.getChild(0);
				Variable var = lookupDeclVar(id.getText());
				assert (var instanceof IntVariable);
				return ((IntVariable) var);
			} else if (term.getChild(0) instanceof IntegerContext) {
				IntegerContext intg = (IntegerContext) term.getChild(0);
				return new SMTInteger(intg.getText());
			}
		} else if (term.getChildCount() == 2) {
			// '-' term
			assert (term.getChild(0).getText().equals("-"));
			TermContext term2 = (TermContext) term.getChild(1);
			Function ret = evaluateTerm(term2);
			return new UnaryMinus(ret);
		} else if (term.getChildCount() == 3) {
			if (term.getChild(0).getText().equals("(")) {
				// '(' term ')'
				TermContext term2 = (TermContext) term.getChild(1);
				return evaluateTerm(term2);
			} else {
				// term '+*-' term
				TermContext lTerm = (TermContext) term.getChild(0);
				TermContext rTerm = (TermContext) term.getChild(2);
				Function lRet = evaluateTerm(lTerm);
				Function rRet = evaluateTerm(rTerm);
				String op = term.getChild(1).getText();
				if (op.equals("+")) {
					return new Plus(lRet, rRet);
				} else if (op.equals("*")) {
					return new Times(lRet, rRet);
				} else if (op.equals("-")) {
					return new Plus(lRet, new UnaryMinus(rRet));
				}
			}
		}
		assert (false);
		return null;
	}

	Function evaluateAssignment(AssignmentContext asgn,
			Set<SideEffect> sideEffects) {
		if (asgn.getChildCount() == 2) {
			if (asgn.getChild(0) instanceof IncrContext) {
				String var = asgn.getChild(1).getText();
				IntVariable declVar = (IntVariable) lookupDeclVar(var);
				Plus ret = new Plus(declVar, new SMTInteger("1"));
				sideEffects.add(new SideEffect(declVar, ret));
				return ret;
			}
			if (asgn.getChild(1) instanceof IncrContext) {
				String var = asgn.getChild(0).getText();
				IntVariable declVar = (IntVariable) lookupDeclVar(var);
				Plus ret = new Plus(declVar, new SMTInteger("1"));
				sideEffects.add(new SideEffect(declVar, ret));
				return declVar;
			}
			if (asgn.getChild(0) instanceof DecrContext) {
				String var = asgn.getChild(1).getText();
				IntVariable declVar = (IntVariable) lookupDeclVar(var);
				Plus ret = new Plus(declVar,
						new UnaryMinus(new SMTInteger("1")));
				sideEffects.add(new SideEffect(declVar, ret));
				return ret;
			}
			if (asgn.getChild(1) instanceof DecrContext) {
				String var = asgn.getChild(0).getText();
				IntVariable declVar = (IntVariable) lookupDeclVar(var);
				Plus ret = new Plus(declVar,
						new UnaryMinus(new SMTInteger("1")));
				sideEffects.add(new SideEffect(declVar, ret));
				return declVar;
			}
		}

		Function rv = null;
		if (asgn.getChild(2) instanceof TermContext) {
			rv = evaluateTerm((TermContext) asgn.getChild(2));
			String var = asgn.getChild(0).getText();
			IntVariable declVar = (IntVariable) lookupDeclVar(var);
			sideEffects.add(new SideEffect(declVar, (IntFunction) rv));
		} else if (asgn.getChild(2) instanceof TestContext) {
			rv = evaluateTest((TestContext) asgn.getChild(2));
			String var = asgn.getChild(0).getText();
			BoolVariable declVar = (BoolVariable) lookupDeclVar(var);
			sideEffects.add(new SideEffect(declVar, (BoolFunction) rv));
		} else if (asgn.getChild(2) instanceof AssignmentContext) {
			rv = evaluateAssignment(((AssignmentContext) asgn.getChild(2)),
					sideEffects);
			String var = asgn.getChild(0).getText();
			Variable declVar = lookupDeclVar(var);
			if (declVar instanceof BoolVariable) {
				sideEffects.add(new SideEffect((BoolVariable) declVar,
						(BoolFunction) rv));
			} else if (declVar instanceof IntVariable) {
				sideEffects.add(new SideEffect((IntVariable) declVar,
						(IntFunction) rv));
			} else {
				assert (false);
			}
		} else {
			assert (false);
		}
		return rv;
	}

	// returns a boolean value
	BoolFunction evaluateTest(TestContext test) {
		if (test.getChildCount() == 1) {
			if (test.getText().equals("true")) {
				return new BoolTrue();
			} else if (test.getText().equals("false")) {
				return new BoolFalse();
			} else {
				if (test.getChild(0) instanceof IdContext) {
					String id = test.getChild(0).getText();
					Variable var = lookupDeclVar(id);
					if (var instanceof BoolVariable) {
						return (BoolVariable) var;
					} else {
						assert (var instanceof IntVariable);
						return new BoolNot(new BoolEquals(var,
								new SMTInteger(0)));
					}
				} else {
					TermContext term = (TermContext) test.getChild(0);
					return new BoolNot(new BoolEquals(new SMTInteger("0"),
							evaluateTerm(term)));
				}
			}
		} else if (test.getChildCount() == 2) {
			TestContext test2 = (TestContext) test.getChild(1);
			return new BoolNot(evaluateTest(test2));
		} else if ((test.getChildCount() == 3)
				&& test.getChild(0).getText().equals("(")
				&& test.getChild(2).getText().equals(")")) {
			return evaluateTest((TestContext) test.getChild(1));
		} else if (test.getChildCount() == 5) {
			String op = test.getChild(3).getText();
			assert (op.equals("%"));

			TermContext lTerm = (TermContext) test.getChild(0);
			IntFunction lrv = evaluateTerm(lTerm);
			TermContext rTerm = (TermContext) test.getChild(2);
			IntFunction rrv = evaluateTerm(rTerm);
			String modulo = test.getChild(4).getText();
			return new BoolDivisible(new SMTInteger(modulo), lrv, rrv);
		} else {
			assert (test.getChildCount() == 3);
			if (test.getChild(0) instanceof TestContext) {
				TestContext lTest = (TestContext) test.getChild(0);
				TestContext rTest = (TestContext) test.getChild(2);
				BoolFunction lrv = evaluateTest(lTest);
				BoolFunction rrv = evaluateTest(rTest);
				String op = test.getChild(1).getText();
				if (op.equals("&&")) {
					return new BoolAnd(lrv, rrv);
				} else if (op.equals("||")) {
					return new BoolOr(lrv, rrv);
				} else {
					assert (false);
					return null;
				}
			} else {
				String op = test.getChild(1).getText();
				TermContext lTerm = (TermContext) test.getChild(0);
				IntFunction lrv = evaluateTerm(lTerm);

				TermContext rTerm = (TermContext) test.getChild(2);
				IntFunction rrv = evaluateTerm(rTerm);
				if (op.equals("!=")) {
					return new BoolNot(new BoolEquals(lrv, rrv));
				} else if (op.equals("==")) {
					return new BoolEquals(lrv, rrv);
				} else {
					return new BoolComp(op, lrv, rrv);
				}
			}
		}
	}

	public void addInputVar(InputVar ret) {
		inputVars.add(ret);
	}

	public IntVariable getPC() {
		return pc;
	}

	public Node getReturnNode() {
		return returnNode;
	}

	public SMTInteger getReturnAddress() {
		return returnAddress;
	}

	public Node getCurState() {
		return curState;
	}

	public void proceed() {
		curState = nextState;
	}

	public void amend(String comment, Set<SideEffect> se) {
		Transition trans = curState.createTrans(comment, se);
		nextState = trans.getNext();
	}

	public void amendWhile(BoolFunction ret) {
		TransitionPair tp = curState.createTrans(ret,
				"while true " + ret.toSMTLIB2(),
				"while false " + ret.toSMTLIB2());
		StatePair whileStackEntry = new StatePair();
		whileStackEntry.checkState = curState;
		whileStackEntry.exitState = tp.falseTrans.getNext();
		whileStack.push(whileStackEntry);
		nextState = tp.trueTrans.getNext();
	}

	public void exitWhile() {
		curState.createTrans(whileStack.peek().checkState);
		nextState = whileStack.peek().exitState;
		whileStack.pop();
	}

	public void breakWhile() {
		curState.createTrans(whileStack.peek().exitState);
		nextState = new Node("after break node", this);
	}

	public void continueWhile() {
		curState.createTrans(whileStack.peek().checkState);
		nextState = new Node("after continue node", this);
	}

	public void amendAssume(BoolFunction ret) {
		TransitionPair tp = curState.createTrans(ret, "assume true",
				"assume false");
		tp.falseTrans.getNext().createTrans(tp.falseTrans.getNext());
		nextState = tp.trueTrans.getNext();
	}

	public void amendAssert(BoolFunction ret) {
		TransitionPair tp = curState.createTrans(ret, "assume true",
				"assume false");
		tp.falseTrans.getNext().createTrans(tp.falseTrans.getNext());
		tp.falseTrans.getNext().setSafetyCondition(new BoolFalse());
		nextState = tp.trueTrans.getNext();
	}

	public void amendIf(BoolFunction ret) {
		TransitionPair tp = curState.createTrans(ret, "if true", "if false");
		ifStack.push(tp.falseTrans.getNext());
		nextState = tp.trueTrans.getNext();
	}

	public void exitIf() {
		nextState = ifStack.pop();
		curState.createTrans(nextState);
	}

	public void exitIfElse() {
		ifStack.push(nextState);
	}

	public void enterElse() {
		Node ret = ifStack.pop();
		nextState = ifStack.pop();
		ifStack.push(ret);
	}

	public void exitElse() {
		nextState = ifStack.pop();
		curState.createTrans(nextState);
	}

	public void returnWith(IntFunction retVal) {
		curState.createReturnTrans(retVal, this);
		nextState = new Node("after return", this);
	}

	public void printYourself(File f) throws IOException {
		TRFileManager fileManager = new TRFileManager(f, getName());
		printStateVariables(fileManager);
		printInputVariables(fileManager);
		CFGConverter converter = new CFGConverter(this);
		converter.print(fileManager);
		// CFGConverter sets the initial node PC
		printInitialCondition(fileManager);
		printArgs(fileManager);
		fileManager.close();
	}

	private void printArgs(TRFileManager fileManager) {
		fileManager.getArgsWriter().println(returnVariable.toSMTLIB2());
		for (Variable v : args) {
			fileManager.getArgsWriter().println(v.toSMTLIB2());
		}
	}

	private void printInitialCondition(TRFileManager fileManager) {
		for (BoolFunction f : initial) {
			fileManager.getInitWriter().println(f.toSMTLIB2());
		}
	}

	private void printInputVariables(TRFileManager fm) {
		Set<String> vars = new HashSet<String>();
		for (InputVar var : inputVars) {
			if (vars.contains(var)) {
				continue;
			}
			fm.getNondetWriter().println(var.getPrintDeclaration());
			vars.add(var.toString());
		}
	}

	private void printStateVariables(TRFileManager fileManager) {
		for (Variable var : stateVariables) {
			if (var instanceof DynArrayVariable) {
				fileManager.getVarWriter().println(
						((IntVariable) ((DynArrayVariable) var).getBound())
								.getPrintDeclaration());
			}
			fileManager.getVarWriter().println(var.getPrintDeclaration());
		}
	}

	public String getName() {
		return name;
	}

	public void addInitial(BoolEquals i) {
		initial.addFirst(i);
	}

	public Set<StateVariable> getStateVariables() {
		return stateVariables;
	}

	public void addStateVariable(StateVariable v) {
		stateVariables.add(v);
	}

	public void addDeclaredVariable(String localName, StateVariable var) {
		varsToDecl.put(localName, var);
		addStateVariable(var);
	}

	public Node getEntryNode() {
		return entryNode;
	}

	public IntVariable getReturnVariable() {
		return returnVariable;
	}

	public void amendFunctionCall(FunctionCallContext ctx) {
		ParseTree possibleArgs;
		String calledName;
		IntVariable assignedVar = null;
		assert (ctx.getChildCount() >= 6);
		assignedVar = (IntVariable) varsToDecl.get(ctx.getChild(0).getText());
		calledName = ctx.getChild(2).getText();
		possibleArgs = ctx.getChild(4);
		List<StateVariable> callArgs = null;
		if (possibleArgs instanceof CallArgsContext) {
			callArgs = evaluateCallArgs((CallArgsContext) possibleArgs);
		}
		Set<SideEffect> se = new HashSet<SideEffect>();
		assert (assignedVar != null);
		SideEffect s = new NondetSideEffect(assignedVar);
		se.add(s);
		FunctionCall call = new FunctionCall(calledName, assignedVar, callArgs);
		calls.put(curState, call);
		amend("after function call " + calledName, se);
	}

	private List<StateVariable> evaluateCallArgs(CallArgsContext args) {
		List<StateVariable> ret = new ArrayList<StateVariable>();
		if (args.getChildCount() == 1) {
			StateVariable f = varsToDecl.get(args.getChild(0).getText());
			ret.add(f);
		} else {
			StateVariable f = varsToDecl.get(args.getChild(0).getText());
			ret.add(f);
			ret.addAll(evaluateCallArgs((CallArgsContext) args.getChild(2)));
		}
		return ret;
	}

	private void parseArg(ArgNameContext ctx) {
		String localName = ctx.getText();

		IntVariable arg = new IntVariable(getName() + "." + localName + ".arg");
		addStateVariable(arg);
		args.add(arg);

		IntVariable var = new IntVariable(getName() + "." + localName);
		addDeclaredVariable(localName, var);

		addInitial(new BoolEquals(arg, var));
	}

	public void parseArgs(FunctionDefArgsContext ctx) {
		if (ctx.getChildCount() == 2) {
			parseArg((ArgNameContext) ctx.getChild(1));
		} else {
			parseArg((ArgNameContext) ctx.getChild(1));
			parseArgs((FunctionDefArgsContext) ctx.getChild(3));
		}
	}

	public FunctionCall getCall(Node n) {
		return calls.get(n);
	}
}
