package cfg;

public interface StateVariable extends Variable {

	StateVariable copy(String newName);
	
	StateVariable getPrime();

}
